# Site defaults for luhj.de, mainly networking
{ pkgs, lib, config, ... }:
{
  config = {
    networking = {
      defaultGateway = "10.2.2.1";
      firewall.checkReversePath = "loose";

      nameservers = [
        "10.2.1.1"
        "10.2.2.1"
      ];

      timeServers = [ "melanchton.luhj.de" ];

      vlans = {
        int = {
          id = 2;
          interface = "bond0";
        };
        sl = {
          id = 4;
          interface = "bond0";
        };
        iot = {
          id = 5;
          interface = "bond0";
        };
      };
    };
  };
}
