{ ... }:
{
  users.groups = {
    produktion.gid = 1005;
  };

  users.users = {
    bastian = {
      description = "Bastian Harendt";
      isNormalUser = true;
      extraGroups = [ "wheel" "docker" ];
      uid = 1000;
      openssh.authorizedKeys.keys = [
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCyWxci4MoFGH/qXGmuKosvtOhgzFjzjcIZfqBHE/4G3dAFZJqaYERPeXmH6Qn69aX9iHrCKX7pS9MelzzUOhrUYPbcHolwO68vmzUHefgZRWvghHhFYYRQ2Cxizykhgj2C5ub0cqKXl14eu5VhT9D2WfV3faDU+veogo5vR35RjhSVbC7ZhF7pGnyie3NA670Yh+LtMRhNEuGc4VE7GD5fvH7ScaJW8Firf7Fblglhdpiir1LeZKMmGe8cHdFtsQInsdUwkgJR0Eicz94Us8bAwOqsqTW5E/BRoKPRVgeMu/acPuIAkEKTm5vimX6VUPY1XKnD4j+voQFbrgE/x6ziLwY5j22dg/Yx5OPw8X5cOGygmx2moRqpqD4N5h/sgUaXPdparSPBJrO1BmUrMqgD7seudVz95zu5Rr8L2oyqYDUosarY3qmqSNEK926IC19/MnTDEgf7/9SXRk3pV2fnGSAwAu+H692iKJc4ooXKllxpyXfdBxr0FojOwW4/SiqNvlkR7FEbJF4ZaVIa/EYp/YP7Fnxmhy2mqPHFvqllt0MKMtPI0PoqOZ9TeX1Yp8Y4bLSp5I2QA5oFi49djUZ8G4LVL8NVu8DDylSpiC6lZq2oxa48nVCF1L3FuxNvZcpqdN87gTBPKoujNmv7S3EoRijbv2NzKEZ+9qKe7jNnuw== bastian@braavos"
      ];
    };
    ck = {
      description = "Christian Kauhaus";
      isNormalUser = true;
      extraGroups = [ "wheel" "docker" ];
      uid = 1003;
      openssh.authorizedKeys.keys = [
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCz7v7YuSEuG4R8Lq6UL6xWB2ew5UMp8zdijRbX8uao9gvMNWbvhbfFE6O8/Pp27H8Hneeqe9nOh/AxeIP+YJU3AM74QWhvqgP/emHThvUzPc9pOILwrLCgTCf8gXzR5Rvsor4XIxNNZj5BKXo71GLbpKBVYWiBNW1Z5YG4pAbcAldWS1LK6zdj6cc94D2Y4S2CGNfafJNW0BG+E0rdTvpoJPfiLMP0b0J+M2SM7fJRUHUZgeYZux44NPP74NC5YcKF7ejoHqiUy4H09Be5ukNhIRuSjeM0dlmiUfAcKndcLA6EuomDZvXCACc7l6Ukn5tC1sSrw7l7VGK0DC65A459 ckauhaus@lionel"
      ];
    };
    produktion = {
      description = "Produktionsteam";
      isNormalUser = true;
      extraGroups = [ ];
      uid = 1005;
      home = "/srv/produktion";
      homeMode = "0755";
    };
  };

  # UID/GID 1006: website
  # UID/GID 1007: ctldap
  # UID/GID 1008: acmesync
  # UID/GID 1009: hyperius
}
