{ pkgs ? import <nixpkgs> {}}:
let
  lib = pkgs.lib;
  nixos-lib = import <nixpkgs/nixos/lib> {};

in
nixos-lib.runTest {
  name = "homeassistant";
  hostPkgs = pkgs;
  nodes = {
    haSrv = { config, pkgs, ... }: {
      imports = [ ../services/homeassistant ];
      config = {
        environment.systemPackages = with pkgs; [ vim ];
        networking = {
          firewall.allowedTCPPorts = [ 22 8123 ];
          domain = "luhj.de";
          hosts = { "127.0.0.1" = [ "ha.luhj.de" "homeassistant.luhj.de" ]; };
        };
        services.nginx.virtualHosts."ha.luhj.de" = {
          forceSSL = lib.mkForce false;
          enableACME = lib.mkForce false;
        };
        services.openssh = {
          enable = true;
          permitRootLogin = "yes";
        };
        users.mutableUsers = false;
        users.users.root.password = "nixos";
        virtualisation.diskSize = 4096;
      };
    };
  };
  interactive.nodes.haSrv = {
    virtualisation.forwardPorts = [
      { from = "host"; host.port = 8022; guest.port = 22; }
      { from = "host"; host.port = 8123; guest.port = 8123; }
    ];
  };
  testScript = { nodes, ... }: ''
    print(">>> ${nodes.haSrv.config.systemd.services.homeassistant.script}")
    haSrv.wait_for_unit("homeassistant.service")
    haSrv.wait_for_open_port(8123)
    haSrv.succeed("curl -v http://localhost:8123/onboarding.html | "
                  "grep /frontend_latest/onboarding")
  '';
}
