{ config, lib, pkgs, modulesPath, ... }:

{
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
  ];

  boot = {
    initrd.availableKernelModules = [
      "ehci_pci" "ahci" "isci" "xhci_pci" "usbhid" "sd_mod"
    ];
    kernelModules = [ "kvm-intel" ];
    loader.grub = {
      enable = true;
      device = "/dev/disk/by-id/ata-Vi550_S3_SSD_20112017701046";
    };
    supportedFilesystems = [ "zfs" "ext4" "xfs" ];
  };

  fileSystems."/" = {
    device = "/dev/disk/by-label/NIXOS";
    fsType = "xfs";
  };

  fileSystems."/srv" = {
    device = "srvpool/srvfs";
    fsType = "zfs";
  };

  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
  swapDevices = [ { label = "SWAP"; } ];
}
