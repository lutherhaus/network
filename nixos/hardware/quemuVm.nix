
# Settings specific to a quemu vm with one disk
# at the moment agricola is the only instance using this
{ lib, ... }:
{
  boot.initrd.availableKernelModules = [ "ata_piix" "uhci_hcd" "ahci" "sd_mod" "sr_mod" ];
  boot.kernelModules = [ ];
  boot.extraModulePackages = [ ];
  swapDevices = [ ];

  nix.maxJobs = lib.mkDefault 8;

  fileSystems."/" =
    { device = "/dev/disk/by-uuid/b1d9bc88-4f24-45cd-a672-47011db09ef7";
      fsType = "ext4";
    };

  # Use the GRUB 2 boot loader.

  boot.loader.grub.enable = true;
  # boot.loader.grub.efiSupport = true;
  # boot.loader.grub.efiInstallAsRemovable = true;
  # boot.loader.efi.efiSysMountPoint = "/boot/efi";
  # Define on which hard drive you want to install Grub.
  boot.loader.grub.device = "/dev/sda"; # or "nodev" for efi only
}
