{ pkgs, lib, ... }:

let
  dataDir = "/zfs/backup/borg";
in
{
  imports = [ ./borgmatic.nix ];

  config = {
    security.sudo.extraRules = [ {
      users = [ "borg" ];
      commands = [
        { command = "/run/wrappers/bin/mount"; options =  [ "NOPASSWD" ]; }
        { command = "/run/wrappers/bin/umount"; options =  [ "NOPASSWD" ]; }
      ];
    } ];

    # Pull backups from Lightshark. All other backups get pushed from the
    # clients.
    services.borgmatic = {
      settings = {
        source_directories = [ "/mnt/lightshark" ];
        repositories = lib.mkForce [
          { path = "${dataDir}/lightshark/repo"; label = "default"; }
        ];
        encryption_passcommand =
          lib.mkForce "cat ${dataDir}/lightshark/encryption_passphrase";
        before_actions = [ "${dataDir}/lightshark/pre.sh" ];
        after_actions = [ "${dataDir}/lightshark/post.sh" ];
      };
    };

    systemd.services.borgmatic-ls1 = {
      description = "Borgmatic backup for Lightshark LS-1";
      serviceConfig = {
        User = "borg";
        ProtectSystem = "yes";
      };
      script = "borgmatic -v0 --syslog-verbosity 2";
      path = with pkgs; [ "/run/wrappers" borgmatic bash cifs-utils ];
      startAt = [ "Sun,Tue,Fri 19:44:07" ];
    };

    # don't use standard systemd service
    systemd.timers.borgmatic-backup.enable = false;
  };
}
