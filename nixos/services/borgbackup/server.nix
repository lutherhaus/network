{ pkgs, lib, ... }:

let
  dataDir = "/zfs/backup/borg";
in
{
  config = {
    services.borgbackup.repos = {
      musculus = {
        authorizedKeys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCjoQPR6k/WE2dhnzAppMwLS5etvR/roAXyTs81NNu0EJY037bwHOjTUykB7xKwm5khVs2pFhi/oZLltufeM3trmSmITv6u3BRcUbUdQyndEyMO0y65wpoYFdE9pqYkkjiJLMjGsa1j/urIDxBCI5xtFYV8LM7qKcIz/jhQnJQarsYgYCu8Ej0hdCjNo63Sl0QfO/4/DVq2vD7hWqeAeOKQfRacBWDZTaAOsZFm3GAJYA0wPK1zSO73IlEghwxUxx8UtHHwmQeap8lTQeIJeMLRdQr+IE2Z43B7OyY3lqfpGRaOdAtexUyTRpfldRJHg4mUH2ekqsB38kzneK284I7fZJxZXhYSvumNiDRkrsbgKl7vfmYeCp4LFqVm8gDPWsenv9z7QLPP/8f776iPEELLQ+SDpkWdew+c3Bka/jxqvojT3NBnOce2zgpUaEAowsU+CQF/KaFSm4mbbq6w6EW57TDJEEy7hO1dckp2aKSLOJcbdhXJ+PnSU4keOEt/pQf8KlsPHewUUKXYT9xxDU7YdubV5QnaUbQqsLy2CGVGyYsf1oknUq3XG4WQUKSYQ2l9rnhI61qrrg7inO0qE6gBC8I6PZI+0d7q3ysdwxVrlQMzRqMB6x2d8zw0LrqoVGZvnnNqwaBqOEYlsRayYR+uKXKBVOyV0COc1c1AAX5JrQ== root@musculus" ] ;
        path = "${dataDir}/musculus" ;
        allowSubRepos = true;
      };
      sl4 = {
        authorizedKeys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCo1XsFdZkI/Gc0MYQOVcCCZvmckYABOkHlLM2GsbrXvMqneYiE8t9gV617za+yzwxRmr5mjGcB6vIKImW2Kfn7p9kFmIwOKc31PaezVTfujuM+rZiheV1tltNXO0rkGVho8kdOa3QiEUO8z4RUcDtXejhy7J0C4N2L0tJgUJfhtqhyT+KUUhZFgF+UpEblnVbcTJleH7y8YAv9idEvDz07ZdZeZUW6M1TFcoiFQlk920/K4rW/aXG5Geoz3gmZI17822epVTKCY9XLDQ1lKlPaINhcS++wZKyE/zTeuuQP1AO1t5mGqJa18bL75josjWeDVx0/HZo8t1ksCwAkoTWNN7VbCEM7DD2Ti/v8Hix4X9eWWsy/FnU27fmt05tNNl5KYtiCBT2wCjGXTlI2QNmAbz/6tK9/zgYJLGbmOaTg4arxdBNuOtcOYVSXEov5dEex2YV6g5Iqi+L60SYsB6pw8MLrrKEL+QvsIyfdfjhf+ahXd1VnMzvSynYsJy9DAck= root@sl4" ] ;
        path = "${dataDir}/sl4";
        allowSubRepos = true;
      };
      aio = {
        authorizedKeys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC6V/Sxq3WmVcu0BoXMYWgRQL9gMuIE7SQHQRIUNAEkBObKm3sdCyqpyLtG2AvQos1thzVzheHTZpegsEJXQITVHlzq1Lxe2bgBTo30uN3JZVfhR2yS6v87n9LjQITl27r+T9ZSdHVRF7BwdHHgabpZMx8lsPbwvX39KaJCrkQEVWuNOw08pwFxbbd0ULk/ePTPOnCTSFrB0Maw/gPLWR/l4U03h5M1Vy/XaJk90LWv2+iDP7VZ3w7lYO/AUOEtjQxPyAUuup+22W65wc7s4MZOjYHFc/PfD9bI80G6vp86P+qSggCD/4UL1YAxbk1adRvWaAtqszGbWWrUEr8ThgvkrY3qmj2w3mxV89V+VEs1mT0sbXvnZ5vJ7iwXEsRZHS4ObluL6ITnfQRZeNfPuaRQiuRgFauhTzkF8tV0ZVmTAlEUu+FSBZFj4WOcWK3NGALGcxRD2gPUd4zhWEzRx7OFJBHDtSTcoMjc3NNYbvKRVfH6M5ancS6b43Iv62987S0= root@aio" ];
        path = "${dataDir}/aio";
        allowSubRepos = true;
      };
      hyperius = {
        authorizedKeys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC9wenNEeY8RPGF6x/hUhsSBmuqye9G8hgkAghUjJnPENiML6cR7ldlStyjH+/pBCXUcv+FDgkKP1kRZCHGnYHn/1OhZT/BXewHvGix69otFPTZQ0PBs01VpBn10e5UY+Ed5O/yfGjEOIqDjPZHT0DL7AQ5/DH3YrPYXuWPVZIEFzKi7S8kuZzyZ8spWhz3J679y6+E+SvVum+yxkF7nsYXJcEqISosV3lkYxMFlTAzhfpozmJESkQXjRjZ4fx+ugNOy+/6zAwhsiNHB6aSkg31b7xi9tLZhcfINheru0WlPvyuG2zQQzC05/yu/22IQUBUJYrZ7HIzAG1d38eWl9AZFhub7eyaPQta81todd/zLsQww10WLYGExn90FTShtI0rw88JXvS4LiL9Gy+frDGa3sI7lhpDvSE+XPyBvDZTFSwyRZZG3lRu86dJVJ/kelJV/OtgtW8K8By8zaaMPatIMDmYEnCNpzXH/hgs1lNVjTTX71B1Mnb+9nB8aTJKksk= root@hyperius" ];
        path = "${dataDir}/hyperius";
        allowSubRepos = true;
      };
      melanchton = {
        authorizedKeys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDal0QiEcKGWJlQEX1sykAuofEqSvUXxKvsHOo4rUOGU6nV48oxmgPzeBFzUrEE833QEMbUHLTQx59swf93eFTrHLIfPS8cDmMA5US9e9mZA7mJX7JpUi2fJpQ6Q2ORFlt99nw38x3yheeyiSo6lP9buGqhr4efGPpDE++mKROBRsQeORPCI1Xaw11OPQPoD5zbgSIu1l7G923kn0FKXdzqXcugTXbi/yDmRLcb4+/L3ZLjbPQsixTBWGQ1XnyK0P53rV+wGsSHnhGMZvWbydolyBnXhtXXb5E+z1BSLi4/kamVqzpVzYcGVWlDJT8OpXzmQenJI6tz+3jV2G2hSnTug5rGc4Bzv6+pRVhxXcTA9wLPAwAzZdNIfMpp3pkJ4zfsPBbOMs++kiDsyTclqlwKWtKW6E9JBHAspM49RiGoTqLFN4OJ8aZWkKkqF1xNteLS0Bmu2QI8TSuFUiY9BY7as38BpvhRQzqw115oSpRA12xI1xZXeNnCDHT3LVdUa38= root@melanchton" ];
        path = "${dataDir}/melanchton";
        allowSubRepos = true;
      };
      calvin = {
        authorizedKeys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCwbkawZ+fNGctg8QCYpwdQbQL9Q/W2bzqg80xHeo68vZyZxRse6dO0l79F5V4+2eMullBfJy9GXrOBmjWTrVchN9TbS4qjELCWxy1jIBZJ2TqGpqY8UnI1ZS6jUmKrJcj2MWmKpcrRp3K96GQquYpjuQHUa2tFAYrRGIh+dqAvEeojyKpVQA8HH3VWvFH3LSrnDQJXeTfeFAyeEOu37dCUpmJMRgB5WlOKwlkzhybggeb2jrkq9LYKc/UzrPNcloP+Al2auiepL5GbmYOtDO8h9C09X67mFJDm/vlz7JB35kTzOuV42Rvy0K4HM5M4omiaTq1QHNlEplC3o7sKuckHha6nFcXf7+1Ydv4noFnAJmvkMG55qYaqte16+EHm4BmEO9jUMD0tp3o+6kuoNs7I26K3fs3UstGIl6TdSLmH92/cBmtZl1E4uNywyB7qimxz72F3XYShLvMS4LJapsEJdoWbmWCWEV81IkAZmEeekdVAJGiQgDqHbdkg6y7Sz0c= root@calvin" ];
        path = "${dataDir}/calvin";
        allowSubRepos = true;
      };
      mrbeam = {
        authorizedKeys = [ "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDkb+zzFuRjpKFYGAFjaoPJykgmt5G3hAf2T/vPs38/W root@mrbeam" ];
        path = "${dataDir}/mrbeam";
        allowSubRepos = true;
      };
    };

    fileSystems."/zfs/backup" = {
      device = "zfs/backup";
      fsType = "zfs";
    };
  };
}
