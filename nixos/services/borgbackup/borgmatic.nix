{ pkgs, lib, config, ... }:

let
  home = "/var/lib/borg";

  stdExcludes = builtins.toFile "borgbackup.exclude" ''
    .cache/
    /swap
    /tmp/*
    /var/cache/*
    /var/tmp/*
  '';

in
{
  config = {
    environment.systemPackages = with pkgs; [ borgbackup ];

    services.borgmatic = {
      enable = true;
      settings = {
        source_directories = [];  # needs to be overriden locally
        repositories = [
          { path = "ssh://borg@bodenstein.luhj.de/./repo"; label = "default"; }
        ];
        one_file_system = true;
        exclude_from = [ stdExcludes ];
        exclude_caches = true;
        exclude_if_present = [ ".nobackup" ];
        exclude_nodump = true;
        encryption_passcommand = "cat ${home}/encryption_passphrase";
        keep_daily = 10;
        keep_weekly = 6;
        keep_monthly = 18;
        keep_yearly = 5;
        checks = [
          { name = "repository"; }
          { name = "archives"; frequency = "2 weeks"; }
        ];
      };
    };

    systemd.services.borgmatic-backup = rec {
      wants = [ "network-online.target" ];
      after = wants;
      script = "borgmatic -v0 --syslog-verbosity 2";
      path = with pkgs; [ bash borgmatic ];
      serviceConfig = {
        Type = "oneshot";
        WorkingDirectory = home;
        Nice = 18;
        IOSchedulingClass = "idle";
      };
    };

    systemd.timers.borgmatic-backup = {
      wantedBy = [ "timers.target" ];
      timerConfig = {
        OnCalendar = "daily";
        RandomizedDelaySec = "3h";
        Persistent = true;
      };
    };

    # The default systemd service does not work well with pre/post-hooks
    systemd.services.borgmatic.enable = false;
    systemd.timers.borgmatic.enable = false;

    users.users.borg = {
      inherit home;
      createHome = true;
      isSystemUser = true;
      group = "borg";
    };

    users.groups.borg = {};
  };
}
