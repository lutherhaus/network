{ pkgs, lib, config, ... }:

let
  website = config.luhj.website.home;
  scripts = "${website}/luhj-website-prod/docker";
  dataDir = "${website}/luhj-website-prod/data";
  sqlDump = "${dataDir}/db_dump/backup.sql";
in
{
  imports = [ ./borgmatic.nix ];

  config = {
    services.borgmatic.settings = {
      source_directories = [ "/" ];
      before_backup = [ ''
        install -o website -g website -m 0640 /dev/null ${sqlDump}
        ${scripts}/dump_db.sh ${sqlDump}
      '' ];
    };
    systemd.services.borgmatic-backup = {
      path = with pkgs; [ docker docker-compose ];
    };
  };
}
