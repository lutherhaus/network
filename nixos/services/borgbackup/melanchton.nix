{ pkgs, lib, config, ... }:

{
  imports = [ ./borgmatic.nix ];

  config = {
    services.borgmatic.settings.source_directories = [
      "/"
      "/srv"
      "/srv/produktion/video"
      "/srv/produktion/ardour"
    ];
  };
}
