# Caddy reverse proxy
# See https://caddyserver.com/docs/
{ ... }:
{
  config = {
    services.caddy = {
      enable = true;
      email = "admin@lutherhaus-jena.de";
      globalConfig = ''
        skip_install_trust
      '';
      logFormat = ''
        level INFO
      '';
    };
  };
}
