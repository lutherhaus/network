{ config, pkgs, lib, ... }:

with builtins;

let
  netw = config.networking;

in
{
  config = {
    networking.firewall.allowedTCPPorts = [ 80 443 ];

    services.logrotate.settings.nginx = {
      files = [ "/var/log/nginx/*.log" ];
      create = "0644 nginx nginx";
    };

    # set default notification address for all defined vhosts
    security.acme = {
      defaults.email = "webmaster@lutherhaus-jena.de";
      acceptTerms = true;
    };

    services.nginx = {
      enable = true;
      appendHttpConfig = ''
        open_log_file_cache max=10;
        log_format proxylog '[$time_iso8601] $request_method '
          '$scheme://$server_name$request_uri '
          'stat=$status reql=$request_length bodyl=$body_bytes_sent '
          'p=$http2$pipe gzip=$gzip_ratio upstat=$upstream_status '
          'upconn=$upstream_connect_time upresp=$upstream_response_time';
        access_log /var/log/nginx/$server_name.log combined;

        proxy_headers_hash_max_size 1024;
      '';
      clientMaxBodySize = "512M";
      recommendedGzipSettings = true;
      recommendedOptimisation = true;
      recommendedProxySettings = true;
      recommendedTlsSettings = true;
      virtualHosts."${netw.hostName}.${netw.domain}" = {
        locations."/".extraConfig =
          "return 301 https://www.lutherhaus-jena.de/;";
      };
    };

    systemd.tmpfiles.rules = [
      "d /var/log/nginx 0755 nginx nginx 180d"
      "d /var/spool/nginx/html 0755"
    ];
  };
}
