{ config, pkgs, lib, ... }:

with builtins;

let
  dataDir = "/srv/unifi";
  vhost = "unifi.luhj.de";
  ip = "10.2.1.5";
  unifiLogs = "${dataDir}/config/logs";

  unifiCompose = toFile "docker-compose.yml" (
    import ./docker-compose.nix { inherit pkgs config dataDir ip; });

  unifiSvc = rec {
    script = "docker-compose up --abort-on-container-exit";
    preStart = "ln -fs ${unifiCompose} ${dataDir}/docker-compose.yml";
    path = with pkgs; [ docker-compose ];
    after = [ "docker.service" "network.target" ];
    requires = after;
    environment = { COMPOSE_FILE = unifiCompose; };
    serviceConfig = {
      Group = "unifi";
      User = "unifi";
      Restart = "on-failure";
      WorkingDirectory = dataDir;
    };
    wantedBy = [ "multi-user.target" ];
  };

in {
  imports = [
    ../docker.nix
    ../nginx.nix
  ];

  config = {
    # https://help.ubnt.com/hc/en-us/articles/218506997-UniFi-Ports-Used
    networking.firewall = {
      allowedUDPPorts = [ 3478 10001 1900 ];
      allowedTCPPorts = [ 8080 8443 8880 8843 6789 ];
    };

    services.logrotate.settings.unifi = {
      files = [ "${unifiLogs}/*.log" ];
      create = "0644 unifi unifi";
      daily = true;
      rotate = 14;
    };

    services.nginx.virtualHosts.${vhost} = {
      default = true;
      enableACME = true;
      forceSSL = true;
      # see https://blog.ljdelight.com/nginx-proxy-to-ubiquiti-unifi-controller/
      locations."/" = {
        proxyPass = "https://${ip}:8443";
        proxyWebsockets = true;
      };
      root = "${dataDir}/www";
    };

    systemd.services.unifi-docker = unifiSvc;

    systemd.tmpfiles.rules = [
      "d ${dataDir} 755 unifi unifi"
      "d ${unifiLogs} 755 unifi unifi"
    ];

    users = {
      groups.unifi.gid = 401;

      users = {
        unifi = {
          description = "UniFi Controller";
          extraGroups = [ "docker" ];
          group = "unifi";
          home = dataDir;
          isSystemUser = true;
          uid = 401;
        };
      };
    };
  };
}
