{ pkgs, config, dataDir, ip }:

with config.users;

''
  services:
    unifi-network-application:
      image: lscr.io/linuxserver/unifi-network-application:8.5.6
      container_name: unifi-network-application
      environment:
        - PUID=${toString users.unifi.uid}
        - PGID=${toString groups.unifi.gid}
        - TZ=Europe/Berlin
        - MONGO_USER=unifi
        - MONGO_PASS=unifi
        - MONGO_HOST=unifi-db
        - MONGO_PORT=27017
        - MONGO_DBNAME=unifi
        - MONGO_AUTHSOURCE=admin
        - MEM_LIMIT=1024
        - MEM_STARTUP=1024
      volumes:
        - ${dataDir}/config:/config
      ports:
        - ${ip}:8443:8443
        - ${ip}:3478:3478/udp
        - ${ip}:10001:10001/udp
        - ${ip}:8080:8080
        - ${ip}:1900:1900/udp
        - ${ip}:8843:8843
        - ${ip}:8880:8880
        - ${ip}:6789:6789
        - ${ip}:5514:5514/udp
      restart: unless-stopped

    unifi-db:
      image: docker.io/mongo:7
      container_name: unifi-db
      environment:
        - MONGO_INITDB_ROOT_USERNAME=root
        - MONGO_INITDB_ROOT_PASSWORD=unifi
        - MONGO_USER=unifi
        - MONGO_PASS=unifi
        - MONGO_DBNAME=unifi
        - MONGO_AUTHSOURCE=admin
      volumes:
        - ${dataDir}/mongodb-data:/data/db
        - ${./init-mongo.sh}:/docker-entrypoint-initdb.d/init-mongo.sh:ro
      restart: unless-stopped
''
