{ pkgs, ... }:
{
  users.users.acmesync = {
    uid = 1008;
    isNormalUser = true;
    group = "nginx";
    # put ssh pubkey into ~acmesync/.ssh/authorized_keys
  };

  services.nextcloud = {
    enable = true;
    hostName = "cloud.luhj.de";
    https = true;
    package = pkgs.nextcloud30;
    caching.redis = true;
    configureRedis = true;
    datadir = "/srv/nextcloud";
    autoUpdateApps.enable = true;
    phpOptions."opcache.interned_strings_buffer" = "23";
    config = {
      dbtype = "pgsql";
      dbuser = "nextcloud";
      dbhost = "/run/postgresql"; # nextcloud will add /.s.PGSQL.5432 by itself
      dbname = "nextcloud";
      adminpassFile = "/var/lib/nextcloud/admin-password";
      adminuser = "ncadmin";
    };
    settings = {
      default_phone_region = "DE";
      log_type = "file";
      overwriteprotocol = "https";
      # calvin
      trusted_proxies = [ "128.140.84.107" "2a01:4f8:c012:e91c::/64" ];
    };
  };

  services.nginx.virtualHosts."cloud.luhj.de" = {
    enableACME = true;
    forceSSL = true;
  };

  services.redis.servers."".enable = true;

  services.postgresql = {
    enable = true;
    ensureDatabases = [ "nextcloud" ];
    ensureUsers = [
     { name = "nextcloud"; ensureDBOwnership = true; }
    ];
  };

  # ensure that postgres is running *before* starting the setup job
  systemd.services.nextcloud-setup = {
    after = ["postgresql.service"];
  };
}
