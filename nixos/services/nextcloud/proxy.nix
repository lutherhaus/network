# Nextcloud access for IPv4 only clients.
# We proxy requests through caddy running on calvin.
# Let's Encrypt certificates are mirrored from melanchton.
{ pkgs, lib, ... }:
{
  imports = [ ../caddy.nix ];

  config =
    let d = "/var/lib/acme/cloud.luhj.de";
    in {
      services.caddy.virtualHosts."cloud.luhj.de" = {
        extraConfig = ''
          tls /var/lib/acme/cloud.luhj.de/fullchain.pem /var/lib/acme/cloud.luhj.de/key.pem
          reverse_proxy https://cloud.luhj.de:443
        '';
      };

      system.activationScripts.acmesync = {
        deps = [ "users" ];
        text = ''
          install -d -o caddy -g caddy ${d}
        '';
      };

      systemd.services."sync-acme-cloud.luhj.de" = {
        script = ''
          rsync -av acmesync@melanchton.luhj.de:${d}/ ${d}/
        '';
        startAt = "12:00:00";
        serviceConfig = {
          Type = "oneshot";
          User = "caddy";
          Group = "caddy";
        };
        path = with pkgs; [ rsync openssh ];
      };
    };
}
