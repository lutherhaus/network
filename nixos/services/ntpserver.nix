{ pkg, lib, ... }:
{
  config = {
    services.chrony = {
      enable = true;
      servers = [
        "0.nixos.pool.ntp.org"
        "1.nixos.pool.ntp.org"
        "2.nixos.pool.ntp.org"
      ];
      serverOption = "iburst";
    };
  };
}
