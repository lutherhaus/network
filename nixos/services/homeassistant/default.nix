{ pkgs, lib, ... }:

let
  haVersion = "2025.1.4";
  home = "/srv/hass";
  configDir = "${home}/config";

  # custom component: ics_calendar
  customIcs = pkgs.fetchFromGitHub {
    owner = "ckauhaus";
    repo = "homeassistant_ics_calendar";
    rev = "a743fd0";
    hash = "sha512-cAjbRrICXef/BgYgVIhc+NBBj9Uor+Fj3X7bzZ3Ar6fd/yfD922+choG1B37XtOd0en9pSJ44pNyTBrHj1mRUg==";
  };

  dockerFile = builtins.toFile "Dockerfile" ''
    FROM homeassistant/home-assistant:${haVersion}
    ADD ics_calendar /src/ics_calendar
    RUN pip install -r /src/ics_calendar/requirements.txt
  '';

  # installs custom components into stock HA docker image
  buildEnv = pkgs.runCommandLocal "homeassistant-docker-build" {} ''
    mkdir -p $out
    cp -r ${customIcs} $out/ics_calendar
    cp ${dockerFile} $out/Dockerfile
  '';

  dockerCompose = pkgs.writeTextFile {
    name = "docker-compose.yaml";
    text = ''
      services:
        homeassistant:
          container_name: homeassistant
          build:
            context: ${buildEnv}
            tags:
              - "homeassistant:${haVersion}-${customIcs.rev}"
          volumes:
            - ${configDir}:/config
            - /etc/localtime:/etc/localtime:ro
            - /etc/passwd:/etc/passwd:ro
            - /etc/group:/etc/group:ro
          devices:
            - /dev/ttyUSB0:/dev/ttyUSB0
          network_mode: host
    '';
  };

in {
  imports = [
    ../docker.nix
    ../mqtt.nix
    ../nginx.nix
  ];

  config = {
    environment.systemPackages = with pkgs; [
      apacheHttpd  # for htpasswd
    ];

    networking.firewall.allowedTCPPorts = [ 8123 ];

    users.users.hass = {
      inherit home;
      uid = 993;
      createHome = true;
      description = "Home Assistant";
      isSystemUser = true;
      shell = pkgs.bashInteractive;
      group = "hass";
      extraGroups = [ "plugdev" "docker" ];
    };

    users.groups = {
      hass.gid = 993;
      plugdev.gid = 400;  # needed inside container
    };

    services.logrotate.settings.home-assistant = {
      files = [ "/srv/hass/config/home-assistant.log" ];
      create = "0644 hass plugdev";
      weekly = true;
      rotate = 4;
      postrotate = ''
        ${pkgs.systemd}/bin/systemctl restart homeassistant.service
      '';
    };

    services.nginx.virtualHosts."ha.luhj.de" = {
      forceSSL = true;
      enableACME = true;
      serverAliases = [ "homeassistant.luhj.de" ];

      locations =
      let wsConfig = ''
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection $connection_upgrade;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
      '';
      in {
        "/" = {
          proxyPass = "http://localhost:8123";
          extraConfig = wsConfig;
        };
      };
    };

    systemd.services.homeassistant = rec {
      preStart = "docker-compose -f ${dockerCompose} build";
      script = "docker-compose -f ${dockerCompose} up " +
        "--abort-on-container-exit";
      path = with pkgs; [ docker-compose ];
      wantedBy = [ "multi-user.target" ];
      after = [ "docker.service" "mosquitto.service" ];
      requires = [ "docker.service" ];
      restartIfChanged = true;
      serviceConfig = {
        Restart = "on-failure";
        User = "hass";
        Group = "plugdev";
        TimeoutStartSec = 600;
      };
    };

    systemd.tmpfiles.rules = [
      "d ${home} 0755 hass hass"
      "Z ${configDir} - hass"
    ];

    system.activationScripts.homeassistant = {
      deps = [ "var" ];
      text = ''
        install -d -o hass -g hass ${configDir}/custom_components
        ln -sfn /src/ics_calendar/custom_components/ics_calendar \
          ${configDir}/custom_components/ics_calendar
      '';
    };
  };
}
