# Online poll/comment/presentation tool
# https://www.claper.co

{ pkgs, lib, config, ... }:

with builtins;

let
  claperVersion = "latest";
  vhost = "frage.lutherhaus-jena.de";
  aliases = [
    "www.frage.lutherhaus-jena.de"
  ];

  smtpPassword = readFile "${config.users.users.website.home}/claper.smtp.pass";
  dockerCompose = pkgs.writeTextDir "claper/docker-compose.yml" (toJSON (
    import ./docker-compose.nix {
      inherit claperVersion vhost smtpPassword;
    }));

  svc = rec {
    script = "docker-compose up --abort-on-container-exit";
    preStop = "docker-compose stop";
    environment = {
      COMPOSE_FILE = "${dockerCompose}/claper/docker-compose.yml";
    };
    path = with pkgs; [ docker-compose docker ];
    after = [ "docker.service" "network.target" ];
    requires = after;
    serviceConfig = {
      Type = "simple";
      Group = "website";
      User = "website";
      Restart = "on-failure";
      WorkingDirectory = "${dockerCompose}/claper";
    };
    wantedBy = [ "multi-user.target" ];
  };

in
{
  imports = [
    ../caddy.nix
    ../docker.nix
  ];

  config = {
    services.caddy.virtualHosts = {
      ${vhost} = {
        serverAliases = aliases;
        extraConfig = ''
          @noncanonical not host ${vhost}
          handle @noncanonical {
            redir https://${vhost}{uri}
          }
          reverse_proxy :4000
        '';
      };
    };

    systemd.services.claper-docker = svc;
  };
}
