{ claperVersion, vhost, smtpPassword }:
{
  version = "3.0";
  services = {
    app = {
      image = "ghcr.io/claperco/claper:${claperVersion}";
      user = "0:0";
      ports = [ "4000:4000"];
      volumes = [ "claper_uploads:/app/uploads" ];
      environment = {
        DATABASE_URL = "postgres://claper:claper@db:5432/claper";
        SECRET_KEY_BASE = "0LZiQBLw4WvqPlz4cz8RsHJlxNiSqM9B48y4ChyJ5v1oA0L/TPIqRjQNdPZN3iEG";
        PRESENTATION_STORAGE = "local";
        MAX_FILE_SIZE_MB = 15;
        ENABLE_ACCOUNT_CREATION = "true";
        ENDPOINT_HOST = vhost;
        ENDPOINT_PORT = 443;
        MAIL_TRANSPORT = "smtp";
        MAIL_FROM = "claper@lutherhaus-jena.de";
        MAIL_FROM_NAME = "Lutherhaus Umfragetool";
        SMTP_RELAY = "wp1186235.mailout.server-he.de";
        SMTP_USERNAME = "wp1186235-claper";
        SMTP_PASSWORD = smtpPassword;
        SMTP_PORT = 587;
        SMTP_TLS = "if_available";
      };
      depends_on.db.condition = "service_healthy";
    };

    db = {
      image = "postgres:15";
      volumes = [ "claper_db:/var/lib/postgresql/data" ];
      environment = {
        POSTGRES_PASSWORD = "claper";
        POSTGRES_USER = "claper";
        POSTGRES_DB = "claper";
      };
      healthcheck = {
        test = [ "CMD-SHELL" "pg_isready -U claper" ];
        interval = "10s";
        timeout = "10s";
        retries = 10;
      };
    };
  };

  volumes = {
    claper_uploads = {};
    claper_db = {};
  };
}
