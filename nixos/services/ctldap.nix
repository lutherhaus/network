{ pkgs, lib, ... }:
# ChurchTools users via LDAP

let
  version = "3.1.2";
  home = "/var/lib/ctldap";
  port = 1389;

  toEnvFile = with lib; attrs: (
    concatStrings (
      mapAttrsToList
        (name: value: "${name}=${escapeShellArg (toString value)}\n")
        attrs
  ));

  staticEnv = pkgs.writeTextFile {
    name = "static.env";
    text = toEnvFile {
      # Add debug infos to log, non-empty string != "false" means "true"!
      DEBUG = true;
      TRACE = false;
      # This is required for clients using lowercase DNs, e.g.
      # ownCloud/nextCloud
      IS_DN_LOWER_CASE = true;
      # This is required for clients that need lowercase email addresses, e.g.
      # Seafile
      IS_EMAIL_LOWER_CASE = true;

      # LDAP admin user, can be a "virtual" root user or a ChurchTools username
      # (virtual root is recommended!)
      # Resulting in cn=root,ou=users,o=churchtools
      LDAP_USER = "root";
      # The static password to be used for the virtual ldapUser, i.e. if that
      # one is NOT a CT account.
      # Ideally, choose a LONG SECURE RANDOM password from a password generator
      # like KeePass and hash it with argon2!
      #LDAP_PW = "<see secrets.env>";
      # LDAP base DN, "o=<xxx>", e.g. "o=churchtools"
      LDAP_BASE_DN = "churchtools";

      # LDAP server ip to listen on, change it to 0.0.0.0 when external access
      # required
      LDAP_IP = "0.0.0.0";
      # LDAP server port, you may change this to the privileged default port 389.
      LDAP_PORT = port;

      # The URI pointing to the root of your ChurchTools installation
      CT_URI = "https://luhj.church.tools";
      # This access token is used to authenticate against ChurchTools for API
      # access.
      # The backing user must be granted sufficient rights for the wrapper to
      # work properly! Typically, these are:
      # churchdb:{ view | view alldata(-1) | view grouptype(-1) | security level person(1,2*) | security level group(1*) }
      # * = additional security levels might be required, depending on your
      # ChurchTools settings.
      # You can obtain the API token from the API:
      # - Login via https://your.ct.domain/api > "General" > "login" (copy your
      # "personId" from the shown output!)
      # - Get your token via "Person" > "/persons/{personId}/logintoken"
      #API_TOKEN = ">>>insert API token here<<<";

      # This controls (in milliseconds) how old the user/group data can be until
      # it is fetched from ChurchTools again
      CACHE_LIFETIME_MS = 300000;
    };
  };

  secretsEnvPath = "${home}/secrets.env";

  dockerCompose = pkgs.writeTextFile {
    name = "docker-compose.yaml";
    text = ''
      version: '3'
      services:
        ctldap:
          image: milux/ctldap:${version}
          ports:
            - "${toString port}:1389"
          env_file:
            - ${staticEnv}
            - ${secretsEnvPath}
    '';
  };

in
{
  imports = [ ./docker.nix ];

  config = {
    users.users.ctldap = {
      inherit home;
      uid = 1007;
      createHome = true;
      description = "ChurchTools LDAP wrapper";
      isSystemUser = true;
      group = "ctldap";
      extraGroups = [ "docker" ];
    };

    users.groups.ctldap.gid = 1007;

    systemd.services.ctldap = rec {
      script = "docker-compose -f ${dockerCompose} up " +
        "--abort-on-container-exit";
      preStart = ''
        if ! [[ -e "${secretsEnvPath}" ]]; then
          echo "Create ${secretsEnvPath} containing LDAP_PW and API_TOKEN."
          echo "See https://github.com/milux/ctldap/blob/${version}/.env.dist"
          exit 1
        fi
      '';
      path = with pkgs; [ docker-compose ];
      wantedBy = [ "multi-user.target" ];
      after = [ "docker.service" ];
      requires = [ "docker.service" ];
      restartIfChanged = true;
      serviceConfig = {
        Restart = "on-failure";
        User = "ctldap";
        Group = "ctldap";
      };
    };
  };
}
