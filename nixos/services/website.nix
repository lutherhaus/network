# Silverstripe web site for luhj.de and related domains.
# Clone the contents of git@gitlab.com:lutherhaus/homepage/docker.nix together
# with submodules into /srv/website/docker.

{ pkgs, lib, config, ... }:

let
  domains = [
    "luhj.de"
    "aufwind-gottesdienst.de"
    "kirche-wenigenjena.de"
    "lutherhaus-jena.de"
    "marienkirche-jena.de"
    "marienkirche-ziegenhain.de"
    "schiller-kirche.de"
    "schillerkirche.de"
    "kaleb-jena.de"
    "hab-mut-zu-deinem-kind.de"
    "helfen-statt-abtreiben.de"
  ];

  allDomains = domains ++ (map (d: "www.${d}") domains);

  home = config.luhj.website.home;

  httpPort = "8000";
  projectDomain = "www.luhj.de";
  projectName = "luhj-website-prod";
  baseDir = "${home}/${projectName}";
  dockerDir = "${baseDir}/docker";
  dataDir = "${baseDir}/data";
  assetsDir = "${dataDir}/assets";
  logDir = "${dataDir}/log";
  dbDumpDir = "${dataDir}/db_dump";

  websiteInit = pkgs.writeShellApplication {
    name = "initialize-website";
    text = let
        requirements = with pkgs; [ coreutils docker-compose git openssh ];
      in ''
        export PATH="${lib.makeBinPath requirements}"

        gitlab_host="git@gitlab.com"
        repo_url="$gitlab_host:lutherhaus/homepage/docker.git"
        repo_dir="${dockerDir}"

        key_file="$HOME/.ssh/id_rsa"
        if [ ! -f "$key_file" ]; then
          echo "Generating SSH key: $key_file"
          ssh-keygen -q -t rsa -b 4096 -N "" -f "$key_file"
        fi

        if ! ssh -i "$key_file" -T "$gitlab_host" > /dev/null; then
          echo "Can't connect to GitLab!"
          echo "Go to each of the following pages:"
          echo "- <https://gitlab.com/lutherhaus/homepage/docker/-/settings/repository>"
          echo "- <https://gitlab.com/lutherhaus/homepage/silverstripe/-/settings/repository>"
          echo "- <https://gitlab.com/lutherhaus/homepage/churchcal-ics-bridge/-/settings/repository>"
          echo "On each of these pages, add or enable the following public SSH key under the"
          echo "'Deploy key' section:"
          cat "$key_file.pub"
          exit 1
        fi

        branch="master"
        if [ ! -e "$repo_dir" ]; then
          git clone --branch "$branch" "$repo_url" "$repo_dir"
        else
          if [ ! -d "$repo_dir" ]; then
            echo "Repository path exists but is not a directory: $repo_dir"
            exit 1
          fi
          actual_repo_url="$(git -C "$repo_dir" remote get-url origin)"
          if [ "$actual_repo_url" != "$repo_url" ]; then
            echo "Repository uses the wrong remote: $actual_repo_url (expected: $repo_url)"
            exit 1
          fi
          git -C "$repo_dir" checkout "$branch"
          git -C "$repo_dir" pull
        fi
        git -C "$repo_dir" submodule update --init --recursive

        cat <<EOF >> "$repo_dir/.env"
        export COMPOSE_PROJECT_NAME="${projectName}"
        export SILVERSTRIPE_DOMAIN="${projectDomain}"
        export HTTP_PORT="${httpPort}"
        export SILVERSTRIPE_ASSETS_DIR_ON_HOST="${assetsDir}"
        export SILVERSTRIPE_LOG_DIR_ON_HOST="${logDir}/silverstripe"
        export DB_DUMP_DIR="${dbDumpDir}"
        EOF

        "$repo_dir/init.sh"

        export COMPOSE_FILE="$repo_dir/docker-compose.yml"
        docker-compose build
      '';
  };

  # results in:
  #   "luhj.de".extraConfig = ''
  #     redir https://www.luhj.de{uri} temporary
  #   '';
  wwwRedirects = lib.listToAttrs (map (domain: { name = domain; value = {
    logFormat = "";
    extraConfig = ''
      redir https://www.${domain}{uri} temporary
    '';
  }; }) domains);

  # custom domain redirect, e.g. aufwind-gottesdienst.de
  redir = from: to: { ${from} = {
    logFormat = "";
    extraConfig = ''
      redir https://${to} temporary
    '';
  }; };

in {
  imports = [
    ./caddy.nix
    ./docker.nix
  ];

  options = with lib; {
    luhj.website.home = mkOption {
      type = types.path;
      default = "/srv/website";
      description = "base path for website";
    };
  };

  config = {
    # ensure web server startup regardless of DNS entries
    networking.hosts = { "127.0.0.1" = allDomains; };

    networking.firewall.allowedTCPPorts = [ 80 443 ];

    users.users.website = {
      inherit home;
      description = "Website";
      isNormalUser = true;
      group = "website";
      extraGroups = [ "docker" ];
      uid = 1006;
      packages = [ websiteInit ];
      homeMode = "755";
    };

    users.groups.website.gid = 1006;

    # SSH keys from gitlab.com (verified by Bastian against the fingerprints
    # listed here: https://gitlab.com/help/instance_configuration)
    services.openssh.knownHosts = {
      "gitlab.com/ecdsa" = {
        hostNames = ["gitlab.com"];
        publicKey = "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFSMqzJeV9rUzU4kWitGjeR4PWSa29SPqJ1fVkhtj3Hw9xjLVXVYrU9QlYWrOLXBpQ6KWjbjTDTdDkoohFzgbEY=";
      };
      "gitlab.com/ed25519" = {
        hostNames = ["gitlab.com"];
        publicKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAfuCHKVTjquxvt6CM6tdG4SLp1Btn/nOeHHE5UOzRdf";
      };
      "gitlab.com/rsa" = {
        hostNames = ["gitlab.com"];
        publicKey = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCsj2bNKTBSpIYDEGk9KxsGh3mySTRgMtXL583qmBpzeQ+jqCMRgBqB98u3z++J1sKlXHWfM9dyhSevkMwSbhoR8XIq/U0tCNyokEi/ueaBMCvbcTHhO7FcwzY92WK4Yt0aGROY5qX2UKSeOvuP4D6TPqKF1onrSzH9bx9XUf2lEdWT/ia1NEKjunUqu1xOB/StKDHMoX4/OKyIzuS0q/T1zOATthvasJFoPrAjkohTyaDUz2LN5JoH839hViyEG82yB+MjcFV5MU3N1l1QL3cVUCh93xSaua1N85qivl+siMkPGbO5xR/En4iEY6K2XPASUEMaieWVNTRCtJ4S8H+9";
      };
    };

    services.caddy = {
      extraConfig = let
        pathRedirects = pathPrefix: vhost: ''
          @${pathPrefix} {
            path_regexp ^/${pathPrefix}(/|$)
            not host ${vhost}
          }
          redir @${pathPrefix} https://${vhost}{uri} temporary
        '';
        in
        ''
          (vhost) {
        ''
        + (pathRedirects "lutherhaus" "www.lutherhaus-jena.de")
        + (pathRedirects "marienkirche" "www.marienkirche-ziegenhain.de")
        + (pathRedirects "schillerkirche" "www.schillerkirche.de")
        + (pathRedirects "gemeindebezirk" "www.kirche-wenigenjena.de")
        + ''
            encode gzip
            reverse_proxy localhost:${httpPort}
          }
        '';

        virtualHosts = wwwRedirects // {
          "test.luhj.de".extraConfig = ''
            reverse_proxy localhost:${httpPort}
          '';
          "www.lutherhaus-jena.de".extraConfig = ''
            redir / /lutherhaus/ temporary
            @gebetswand {
              path /gebetswand /gebetswand/
            }
            redir @gebetswand https://hackmd.io/MvI2ePM5Tgaigio2-wnrcg?edit temporary
            # historic stuff
            redir /schiller_k.html https://www.schillerkirche-jena.de/ temporary
            redir /i_lhbilder.html https://www.lutherhaus-jena.de/lutherhaus/ temporary

            import vhost
          '';
          "www.marienkirche-ziegenhain.de".extraConfig = ''
            redir / /marienkirche/ temporary
            redir /marienkirche/ueber-uns/die-kirche/ /marienkirche/geschichte/ temporary
            import vhost
          '';
          "www.schillerkirche.de".extraConfig = ''
            redir / /schillerkirche/ temporary
            import vhost
          '';
          "www.kirche-wenigenjena.de".extraConfig = ''
            redir / /gemeindebezirk/ temporary
            import vhost
          '';
          "www.kaleb-jena.de" = {
            serverAliases = [
              "www.hab-mut-zu-deinem-kind.de"
              "www.helfen-statt-abtreiben.de"
            ];
            extraConfig = ''
              @isAliasHost `{host} != "www.kaleb-jena.de"`
              redir @isAliasHost https://www.kaleb-jena.de{uri}

              encode gzip
              root * /srv/website/kaleb-jena.de/htdocs
              file_server
            '';
          };
        }
        // (redir "www.aufwind-gottesdienst.de" "www.lutherhaus-jena.de/lutherhaus/gottesdienst/aufwind/")
        // (redir "ct.lutherhaus-jena.de" "luhj.church.tools")
        // (redir "churchtools.lutherhaus-jena.de" "luhj.church.tools")
        // (redir "www.schiller-kirche.de" "www.schillerkirche.de")
        // (redir "www.marienkirche-jena.de" "www.marienkirche-ziegenhain.de")
        // (redir "www.luhj.de" "www.lutherhaus-jena.de")
      ;
    };
  };
}
