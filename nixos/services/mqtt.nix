{ pkgs, ... }:
{
  environment.systemPackages = with pkgs; [ mosquitto ];

  networking.firewall.allowedTCPPorts = [ 1883 ];

  services.mosquitto = {
    enable = true;
    listeners = [ {
      acl = [
        "topic read $SYS/#"
        "pattern write $SYS/broker/connection/%c/state"
      ];
      users = {
        "shelly-ventilation" = {
          acl = [ "shellies/#" "homeassistant/#" ];
          hashedPassword =
            "$7$101$RmqbNfVxggCwO9wF$BoWGqhlde7FAgx/rulTv8dAgrEGBpekJE/ZC2Ybvf5pAmfmykBYA0mskVXRijxFggQxNTxiYToBABAdXluu5OA==";
        };
        "tallylight" = {
          acl = [ "#" ];
          hashedPassword =
            "$7$101$cy+CYfemR6oOjYV6$wVxGFm8hf+6JzxK+TS6QIT/mJQ7St6DBJQDsFHRisSGeS2aOF60F2/cIFiMq7gm1q0p2pX5ke5VV5ILRK+md6Q==";
        };
        "obs" = {
          acl = [ "#" ];
          hashedPassword =
            "$7$101$3ZtvDy39cC6YpHsk$YhgVQi8wTYsJinhFof3tuWqfBi9Lnzr9uIV/EW5e0dCBu75cwgjoYBIC1bNCNNqzm6Nl63vaQpnlPZTohnpBrw==";
        };
        "onair" = {
          acl = [ "onair/#" "shellies/#" "homeassistant/#"  ];
          hashedPassword =
            "$7$101$cOtpva+CGNO30mlo$Y++MCAp+q7EUai/CFPF0hAZXA/g1dZsYe+LI8Emn2sv1F26n9aGTi9iMEc/eFkcLWnDBgCBLZ3VgMxh4WOMrSA==";
        };
        "tally-totale" = {
          acl = [ "tally-totale/#" ];
          hashedPassword =
            "$7$101$DxSz1vNGIGCoF1EM$8hSDofWJQDX6jJqhAtwvf0dXr2ednjfDaPGGI/G1pUcaAL1XXvjTWOYXpFPHrbjToP2Ix9km2hct0RDbBXdiYg==";
        };
        "hass" = {
          acl = [ "#" ];
          hashedPassword =
            "$7$101$vkSMIiBsBfaGltof$kGa/JDWCM4HJ+uBRdIFCOPzOaLiTlpBmGMlKHkv/2ixggEP1G8BDkxzysKBKIwWjTuGil8SSmPDflMsxt/1TZQ==";
        };
        "tally3" = {
          acl = [ "tally3/#" "homeassistant/#" ];
          hashedPassword =  # SisUvTacoon4
            "$7$101$dJ0Dk8fSJISMgnVO$GTzDwxf/GowZykCG8rsB6Obp4B2ZaI5e4NUd6HiYkvdHgZ8xYErWPK+ZxNPB7jdN30G7AIs1ekymiXFRIJHvWQ==";
        };
        "tally4" = {
          acl = [ "tally4/#" "homeassistant/#" ];
          hashedPassword =  # SisUvTacoon4
            "$7$101$dJ0Dk8fSJISMgnVO$GTzDwxf/GowZykCG8rsB6Obp4B2ZaI5e4NUd6HiYkvdHgZ8xYErWPK+ZxNPB7jdN30G7AIs1ekymiXFRIJHvWQ==";
        };
        "BSB-LAN" = {
          acl = [ "BSB-LAN/#" "homeassistant/#" ];
          hashedPassword =
            "$7$101$FIopPvJkCE9vsRz9$wiZnL/R7KLAYCunqV2ovTv2FRDssMbYPR6bwamfEfi6l+8bXk2YFyhLQa2IQPKDOV77eU0U53h9Y+qmtiyG/Pw==";
        };
      };
    } ];
  };
}
