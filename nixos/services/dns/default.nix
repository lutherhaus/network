{ pkgs, lib, ...}:

with builtins;

let
  hetznerSecondaries = [
    # ns1.first-ns.de
    "213.239.242.238"
    "2a01:4f8:0:a101::a:1"
    # robotns2.second-ns.de
    "213.133.100.103"
    "2a01:4f8:0:1::5ddc:2"
    "213.133.105.6"         # switch off on 2023-12-04
    "2a01:4f8:d0a:2004::2"  # switch off on 2023-12-04
    # robotns3.second-ns.com
    "193.47.99.3"
    "2001:67c:192c::add:a3"
  ];
  genericZones = [
    "aufwind-gottesdienst.de"
    "kirche-wenigenjena.de"
    "schiller-kirche.de"
    "schillerkirche.de"
  ];
  kalebZones = [
    "kaleb-jena.de"
    "hab-mut-zu-deinem-kind.de"
    "helfen-statt-abtreiben.de"
  ];

in
{
  config = {
    environment.systemPackages = with pkgs; [ dig ];

    networking.firewall.allowedUDPPorts = [ 53 ];
    networking.firewall.allowedTCPPorts = [ 53 ];

    services.bind = {
      enable = true;

      extraConfig = ''
        include "/etc/bind/luhj-nsupdate.key";
      '';

      cacheNetworks = [
        "127.0.0.0/8"
        "10.0.0.0/8"
        "172.16.0.0/12"
        "192.168.0.0/16"
        "5.9.65.64/27"
        "2a01:4f8:161:504e::/64"
      ];

      zones =
        listToAttrs (map (d: lib.nameValuePair d {
          master = true;
          slaves = hetznerSecondaries;
          file = ./generic.zone;
        }) genericZones)
      // listToAttrs (map (d: lib.nameValuePair d {
          master = true;
          slaves = hetznerSecondaries;
          file = ./kaleb.zone;
        }) kalebZones)
      // {
        "luhj.de" = {
          master = true;
          slaves = hetznerSecondaries;
          file = ./luhj.de.zone;
        };
        "dyn.luhj.de" = {
          master = true;
          slaves = hetznerSecondaries;
          file = "/var/db/bind/dyn.luhj.de.zone";
        };
        "0.a.0.7.0.7.4.0.1.0.0.2.ip6.arpa" = {
          master = true;
          slaves = hetznerSecondaries;
          file = ./0.a.0.7.0.7.4.0.1.0.0.2.ip6.arpa.zone;
        };
      };
    };

    system.activationScripts.bind = {
      deps = [ "etc" "users" ];
      text = ''
        install -o named -g named -d /var/db/bind
        if ! [[ -e /var/db/bind/dyn.luhj.de.zone ]]; then
          cat > /var/db/bind/dyn.luhj.de.zone <<__EOT__
        \$INCLUDE ${./luhj.de.zone}
        __EOT__
        fi
        chown named: /var/db/bind/dyn.luhj.de.zone 
      '';
    };
  };
}
