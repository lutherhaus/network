# Generic NFS server w/firewall config.
# Add `services.nfs.exports` on individual servers.
{ pkgs, ... }:
{
  networking.firewall = rec {
    allowedTCPPorts = [ 111 ];
    allowedUDPPorts = allowedTCPPorts;
    allowedTCPPortRanges = [ { from = 2049; to = 2052; } ];
    allowedUDPPortRanges = allowedTCPPortRanges;
  };

  services.nfs.server = {
    enable = true;
    # put nfsd and related services into a compact port range
    statdPort = 2050;
    lockdPort = 2051;
    mountdPort = 2052;
  };

  systemd.services.nfs-export = rec {
    script = "${pkgs.nfs-utils}/bin/exportfs -rav";
    serviceConfig = {
      Type = "oneshot";
    };
    after = [ "nfs-server.service" "network-online.target" ];
    requires = after;
    wantedBy = [ "multi-user.target" ];
  };
}
