# Long-lived VM which runs DNS, website and mailing list services.

{ pkgs, config, modulesPath, ... }:

{
  imports = [
    (modulesPath + "/profiles/qemu-guest.nix")
    ../generic
    ../services/borgbackup/calvin.nix
    ../services/claper
    ../services/dns
    ../services/nextcloud/proxy.nix
    ../services/website.nix
  ];

  config = {
    boot = {
      growPartition = true;
      loader.grub.device = "/dev/sda";
      initrd.availableKernelModules = [
        "ata_piix" "uhci_hcd" "xen_blkfront" "vmw_pvscsi"
      ];
      initrd.kernelModules = [ "nvme" ];
    };

    fileSystems = {
      "/" = {
        device = "/dev/sda1";
        autoResize = true;
        fsType = "ext4";
      };
      "/srv" = {
        device = "/dev/sdb";
        fsType = "xfs";
      };
    };

    networking = {
      hostName = "calvin";
      hostId = "67bf68f4";
      usePredictableInterfaceNames = false;

      interfaces.eth0 = {
        useDHCP = true;  # 128.140.84.107
        ipv6 = {
          addresses = [ {
            address = "2a01:4f8:c012:e91c::1";
            prefixLength = 64;
          } ];
          routes = [ {
            address = "::";
            prefixLength = 0;
            via = "fe80::1";
          } ];
        };
      };

      nameservers = [
        "2a01:4ff:ff00::add:1"
        "2a01:4ff:ff00::add:2"
      ];
    };

    services.qemuGuest.enable = true;

    system.stateVersion = "21.11";

    swapDevices = [
      { device = "/swap"; size = 2048; }
    ];
  };
}
