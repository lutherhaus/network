{ pkgs, ... }:

{
  imports = [
    ../generic
    ../hardware/bodenstein.nix
    ../site/luhj.nix
    ../services/borgbackup/bodenstein.nix
    ../services/borgbackup/server.nix
  ];

  config = {
    boot.supportedFilesystems = [ "xfs" "zfs" "ext4" ];

    environment.systemPackages = with pkgs; [
      zfs zfstools
    ];

    fileSystems = {
      "/zfs" = {
        device = "zfs";
        fsType = "zfs";
      };
    };

    networking = {
      hostName = "bodenstein";
      hostId = "50d41e9b"; # needed for zfs

      bonds.bond0 = {
        interfaces = [ "enp3s0" "enp5s0" ];
        driverOptions = {
          mode = "802.3ad";
          lacp_rate = "fast";
        };
      };

      # see also services/unifi/config.gateway.nix for static addresses
      interfaces = {
        bond0.ipv4.addresses = [ { address = "10.2.1.9"; prefixLength = 24; } ];
        int.ipv4.addresses = [ { address = "10.2.2.9"; prefixLength = 24; } ];
        sl.ipv4.addresses = [ { address = "10.2.4.9"; prefixLength = 24; } ];
        iot.ipv4.addresses = [ { address = "10.2.5.9"; prefixLength = 24; } ];
        enp3s0.wakeOnLan.enable = true;
        enp5s0.wakeOnLan.enable = true;
      };
    };

    services.zfs.autoScrub.enable = true;

    system.stateVersion = "21.11";
  };
}
