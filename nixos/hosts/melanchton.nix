{ pkgs, ... }:
{
  imports = [
    ../generic
    ../hardware/melanchton.nix
    ../site/luhj.nix
    ../services/borgbackup/melanchton.nix
    ../services/ctldap.nix
    ../services/homeassistant
    ../services/nextcloud
    ../services/nfs.nix
    ../services/ntpserver.nix
    ../services/unifi
  ];

  config = {
    environment.systemPackages = with pkgs; [
      zfstools
    ];

    networking = {
      hostName = "melanchton";
      hostId = "23b1a2d4"; # needed for zfs

      bonds.bond0 = {
        interfaces = [ "eno1" "enp8s5" ];
        driverOptions = {
          mode = "802.3ad";
          lacp_rate = "fast";
        };
      };

      hosts = {
        # needed for NFS export
        "10.2.4.6" = [ "audiorec.luhj.de" "audiorec" ];
        "10.2.4.8" = [ "sl4.luhj.de" "sl4" ];
      };

      # see also services/unifi/config.gateway.nix for static addresses
      interfaces = {
        # cannot use DHCP here because we run the Unifi controller
        bond0.ipv4.addresses = [ { address = "10.2.1.5"; prefixLength = 24; } ];
        int.ipv4.addresses = [
          { address = "10.2.2.5"; prefixLength = 24; }
          # cloud.luhj.de
          { address = "10.2.2.15"; prefixLength = 24; }
        ];
        sl.ipv4.addresses = [
          # NFS, HA
          { address = "10.2.4.5"; prefixLength = 24; }
          # MQTT
          { address = "10.2.4.15"; prefixLength = 24; }
        ];
        iot.ipv4.addresses = [ { address = "10.2.5.5"; prefixLength = 24; } ];
      };
    };

    services.nfs.server.exports = ''
      /srv/produktion/ardour audiorec(rw,no_subtree_check)
      /srv/produktion/video sl4(rw,no_subtree_check)
    '';

    services.zfs.autoScrub.enable = true;

    system.stateVersion = "22.11";

    users.users.hyperius = {
      home = "/srv/hyperius";
      isSystemUser = true;
      description = "Temporary account for hyperius backup";
      createHome = true;
      useDefaultShell = true;
      uid = 1009;
      group = "users";
      openssh.authorizedKeys.keys = [
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC+m9y7SnbU5Y1Na8jrro3R9fQJXo8bwuPnAbGs9HtHz6E6qX8VM0Jf2XqWEjKq2RtdSg8sRzNiv6P7M+mqVSK++0rdvkpaAZk6CyB2hC0Q5LyueTMG/x7cIS7nYAKePkS/bNl3Y52407aowEMiPOXU8gJuLmZbsReeFtXXEO9vMU8RJA2MhuU2qwtDGML0/hLqYp3NSCIacnEpguhGUYFIQPcGYAJah1a0BQDpbD/K7fLzvvFawxd/FqQhctfTy7IGGV3dhhBP90YE9277O4PY29Vl+LzG6f9Wn0fanVX+IfJbThxuvp1wFfl5By0Cd08XZsttVen3pJIpp+Yk352ZGP6/stNwRw5wqaU+DY894BQY3OJ1ngXsoxzqWWT6T69/dnWYAqtsXvg/PNdRWM5K5mVK1ymnDTl2UEYdnzMNemNK2z8AxiP6n3ZLFndiEp+zbAbrmObhf8NCAqdW5Z7+Z/awH1LIvxBc+ziPV2E/JBNu/1evolSba3HibTJnm3s= root@hyperius"
      ];
    };
  };
}
